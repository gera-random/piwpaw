﻿using System;

namespace piwpau
{
    public class Utils
    {
        public static float RandomFloat(float minimum, float maximum)
        { 
            Random random = new Random();
            return (float)random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}