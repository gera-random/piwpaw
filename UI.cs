using Godot;
using System;

public class UI : Control
{
    public void UpdateBulletCount(int bulletCount)
    {
        GetNode<Label>("BulletCount").Text = bulletCount.ToString();
    }

    public void UpdateWeapon(int slot, PackedScene weapon)
    {
        var label = GetNode<Label>("CurrentWeapon");
        label.Text = $"{slot}: ";
        if (weapon != null)
            label.Text += $"{weapon.Instance().Name}";
        else
            label.Text += "No weapon";
    }

    public void UpdateHealth(int health)
    {
        if (Owner.HasNode("Player") && health > 0)
            GetNode<Label>("Health").Text = $"HP: {health}";
        else
            GetNode<Label>("Health").Text = $"NO MORE";
    }
}