using Godot;
using Object = Godot.Object;
using piwpau;

public class GameWorld : Node2D
{
    [Export] public AudioStream StoneCrackSound;
    [Export] public AudioStream GhostDisappearSound;
    
    public override void _Ready()
    {
        VisualServer.SetDefaultClearColor(new Color(Utils.RandomFloat(0.3f,0.5f),
            Utils.RandomFloat(0.3f,0.5f), Utils.RandomFloat(0.3f,0.5f), 0.5f));

        var player = GetNode<Player>("Player");
        GD.Print(player.GetType());
        var ui = GetNode<UI>("CanvasLayer/UI");
        
        player.Connect(nameof(Player.BulletCountUpdated), ui, nameof(ui.UpdateBulletCount));
        player.Connect(nameof(Player.WeaponUpdated), ui, nameof(ui.UpdateWeapon));
        player.Connect(nameof(Player.HealthUpdated), ui, nameof(ui.UpdateHealth));
        player.EmitSignal(nameof(Player.BulletCountUpdated), player.Bullets);
        player.EmitSignal(nameof(Player.HealthUpdated), player.Health);
        player.EmitSignal(nameof(Player.WeaponUpdated), player.NCurrentWeapon, player.Weapons[player.NCurrentWeapon]);
        
        GetNode<Stone>("Stone").Connect(nameof(Stone.Cracked), this, nameof(OnStoneCracked));
    }

    public override void _Input(InputEvent @event)
    {
        if (@event.IsActionPressed("toggle_fullscreen"))
        {
            OS.WindowFullscreen = !OS.WindowFullscreen;
        }
        if (@event.IsActionReleased("exit"))
        {
            if (!HasNode("Player"))
            {
                GetTree().ReloadCurrentScene();
                return;
            }
            GetTree().Quit();
        }
    }

    public void OnStoneCracked(Vector2 position)
    {
        if (StoneCrackSound != null)
        {
            PlaySoundAtPos(position, StoneCrackSound);
        }
    }
    
    public void OnGhostDefeated(Vector2 position)
    {
        if (StoneCrackSound != null)
        {
            PlaySoundAtPos(position, GhostDisappearSound);
        }
    }

    private void PlaySoundAtPos(Vector2 position, AudioStream audioStream)
    {
        var soundPlayer = new AudioStreamPlayer2D();
        soundPlayer.Stream = audioStream;
        soundPlayer.Position = position;
        soundPlayer.VolumeDb = -10;
        AddChild(soundPlayer);
        soundPlayer.Play();
    }

    public void OnGhostSpawnerBodyEntered(Object body)
    {
        if (body.HasMethod(nameof(Player.TakeDamage)))
        {
            GetNode<Door>("GateDoor1").Open();
            GetNode<Door>("GateDoor2").Open();
        }
    }
}
