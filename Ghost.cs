using Godot;
using System;

public class Ghost : KinematicBody2D, IHittable
{
    [Export] public int Speed = 2;
    [Export] public int Health = 10;
    [Export] public int Damage = 50;
    
    [Signal]
    public delegate void Defeated(Vector2 position);
    
    private Vector2 velocity;
    
    public override void _PhysicsProcess(float delta)
    {
        if (GetParent().HasNode("Player"))
        {
            var player = GetParent().GetNode<Player>("Player");
            var dir = player.GlobalPosition - GlobalPosition;
            Rotation = dir.Angle();
            velocity = new Vector2(Speed, 0).Rotated(Rotation);
            var collision = MoveAndCollide(velocity);
            if (collision != null)
            {
                OnCollision(collision);
            }
        }
    }

    private void OnCollision(KinematicCollision2D collision)
    {
        if (collision.Collider.HasMethod("TakeDamage"))
        {
            var isPlayerDead = (bool)collision.Collider.Call("TakeDamage", Damage);
            if (!isPlayerDead)
                QueueFree();
        }
    }

    public bool Hit(int damage)
    {
        GetNode<AudioStreamPlayer2D>("HitSoundPlayer").Play();
        Health -= damage;
        if (Health <= 0)
        {
            QueueFree();
            EmitSignal(nameof(Defeated), Position);
        }
        return true;
    }
}
