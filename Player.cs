using Godot;

public class Player : KinematicBody2D
{
    [Export] public int Speed = 200;
    [Export] public float Reload = 0.2f;
    [Export] public float Bullets = 10;
    [Export] public int PickupRadius = 200;
    [Export] public int Health = 100;
    
    public PackedScene[] Weapons =
    {
        null,
        (PackedScene) GD.Load("res://Bullet.tscn"),
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    };

    public int NCurrentWeapon = 1;

    Vector2 _velocity;

    private Timer _timer = new Timer();
    private bool _canShoot = true;
    
    [Signal]
    public delegate void BulletCountUpdated(int bulletCount);
    [Signal]
    public delegate void HealthUpdated(int health);
    [Signal]
    public delegate void WeaponUpdated(int slot, PackedScene weapon);
    
    public override void _Ready()
    {
        base._Ready();
        
        AddChild(_timer);
        _timer.OneShot = true;
        _timer.WaitTime = Reload;
        _timer.Connect("timeout", this, nameof(OnShootTimeout));
    }
    
    public void OnShootTimeout()
    {
        _canShoot = true;
    }
    
    public void GetInput()
    {
        _velocity = new Vector2();
    
        if (Input.IsActionPressed("ui_right"))
            _velocity.x += 1;
    
        if (Input.IsActionPressed("ui_left"))
            _velocity.x -= 1;
    
        if (Input.IsActionPressed("ui_down"))
            _velocity.y += 1;
    
        if (Input.IsActionPressed("ui_up"))
            _velocity.y -= 1;

        if (Input.IsActionPressed("ui_up"))
            _velocity.y -= 1;
        
        if (Input.IsActionPressed("shoot") && _canShoot && Bullets > 0)
        {
            Shoot();
            _canShoot = false;
            _timer.Start();
        }

        if (Input.IsActionJustPressed("next_weapon"))
            SwitchWeapon(+1);
        
        if (Input.IsActionJustPressed("prev_weapon"))
            SwitchWeapon(-1);

        if (Input.IsActionJustPressed("drop_weapon"))
            DropCurrentWeapon();
        
        _velocity = _velocity.Normalized() * Speed;

        //Rotate(GetAngleTo(GetGlobalMousePosition()));
    }
    
    public override void _PhysicsProcess(float delta)
    {
        GetInput();
        _velocity = MoveAndSlide(_velocity);
        
        var dir = GetGlobalMousePosition() - GlobalPosition;
        
        // Don't move if too close to the mouse pointer
        if (dir.Length() > 5)
        {
            Rotation = dir.Angle();
            _velocity = MoveAndSlide(_velocity);
        }
    }
    public void Shoot()
    {
        if (Weapons[NCurrentWeapon] != null)
        {
            var p = (Projectile)Weapons[NCurrentWeapon].Instance();
       
            if (Bullets >= p.Cost)
            {
                // "Muzzle" is a Position2D placed at the barrel of the gun
                p.Start(GetNode<Node2D>("Muzzle").GlobalPosition, Rotation);
                GetParent().AddChild(p);

                GetNode<AudioStreamPlayer2D>("ShootSoundPlayer").Play();
                Bullets -= p.Cost;

                EmitSignal(nameof(BulletCountUpdated), Bullets);
            }
        }
    }

    public void PickupAmmo(PackedScene ammo)
    {
        GetNode<AudioStreamPlayer2D>("PickupSoundPlayer").Play();
        
        AddNextWeapon(ammo, true);
    }

    private void AddNextWeapon(PackedScene weapon, bool selectNewWeapon)
    {
        for (int i = NCurrentWeapon; i < NCurrentWeapon + Weapons.Length; i++)
        {
            if (Weapons[i % Weapons.Length] == null)
            {
                Weapons[i % Weapons.Length] = weapon;
                
                if (selectNewWeapon)
                {
                    NCurrentWeapon = i % Weapons.Length;
                    EmitSignal(nameof(WeaponUpdated), NCurrentWeapon, Weapons[NCurrentWeapon]);
                }
                
                break;
            }
        }
    }

    public void SwitchWeapon(int offset)
    {
        SwitchToWeapon((Weapons.Length + NCurrentWeapon + offset) % Weapons.Length);
    }

    public void SwitchToWeapon(int n)
    {
        NCurrentWeapon = n;
        EmitSignal(nameof(WeaponUpdated), NCurrentWeapon, Weapons[NCurrentWeapon]);
        
        if (Weapons[NCurrentWeapon] != null)
            GetNode<AudioStreamPlayer2D>("SwitchWeaponSoundPlayer").Play();
    }

    public bool SwitchToNextWeapon()
    {
        for (int i = NCurrentWeapon; i < NCurrentWeapon + Weapons.Length; i++)
        {
            if (Weapons[i % Weapons.Length] != null)
            {
                SwitchToWeapon(i % Weapons.Length);
                return true;
            }
        }

        return false;
    }

    public void DropCurrentWeapon()
    {
        var weaponScene = Weapons[NCurrentWeapon];
        if (weaponScene != null)
        {
            var weaponInstance = (Projectile) weaponScene.Instance();
            if (weaponInstance.Pickable != null && Position.DistanceTo(GetGlobalMousePosition()) < PickupRadius)
            {
                var pickableInstance = (Node2D)weaponInstance.Pickable.Instance();
                GetParent().AddChild(pickableInstance);
                pickableInstance.Position = GetGlobalMousePosition();
                
                Weapons[NCurrentWeapon] = null;
                GetNode<AudioStreamPlayer2D>("PickupSoundPlayer").Play();

                if (!SwitchToNextWeapon())
                {
                    EmitSignal(nameof(WeaponUpdated), NCurrentWeapon, Weapons[NCurrentWeapon]);
                }
            }
        }
    }
    
    public void PickupBullets(int bulletCount)
    {
        Bullets += bulletCount;
        GetNode<AudioStreamPlayer2D>("PickupSoundPlayer").Play();
        
        EmitSignal(nameof(BulletCountUpdated), Bullets);
    }

    public bool TakeDamage(int damage)
    {
        Health -= damage;

        EmitSignal(nameof(HealthUpdated), Health);
        
        if (Health <= 0)
        {
            QueueFree();
            return true;
        }

        return false;
    }
}