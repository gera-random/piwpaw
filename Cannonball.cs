﻿using System.Collections.Generic;
using Godot;

public class Cannonball : Projectile
{
    [Export] public PackedScene Piece =  (PackedScene)GD.Load("res://Bullet.tscn");
    [Export] public int NPieces = 5;

    public Cannonball()
    {
        Cost = 5;
    }
    
    public override void OnCollision(KinematicCollision2D collision)
    {
        for (int i = 0; i < NPieces; i++)
        {
            var pieceInstance = (Projectile) Piece.Instance();
            float dAngle = 360f / NPieces;
            pieceInstance.Start(GlobalPosition, Rotation+i*dAngle);
            GetParent().AddChild(pieceInstance);
        }

        QueueFree();
    }
}