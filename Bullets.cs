using Godot;
using Object = Godot.Object;

public class Bullets : Area2D
{
    [Export] public int BulletCount = 25;

    public void OnBulletsBodyEntered(Object body)
    {
        if (body.HasMethod(nameof(Player.PickupBullets)))
        {
            body.Call(nameof(Player.PickupBullets), BulletCount);
            QueueFree();
        }
    }
}
