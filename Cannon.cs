﻿using Godot;
using Object = Godot.Object;

public class Cannon : Area2D
{
    [Export] public int BulletCount = 25;

    private PackedScene cannonball = (PackedScene) GD.Load("res://Cannonball.tscn");

    public void OnCannonBodyEntered(Object body)
    {
        if (body.HasMethod(nameof(Player.PickupAmmo)))
        {
            body.Call(nameof(Player.PickupAmmo), cannonball);
            QueueFree();
        }
    }
}