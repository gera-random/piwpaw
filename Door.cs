using Godot;
using System;

public class Door : StaticBody2D
{
    public bool Hit(int damage)
    {
        GetNode<AudioStreamPlayer2D>("HitSoundPlayer").Play();
        return false;
    }

    public void Open()
    {
        GetNode<AnimationPlayer>("CollisionShape2D/AnimationPlayer").Play("Open");
    }
}
