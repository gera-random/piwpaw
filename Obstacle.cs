using Godot;
using System;

public class Obstacle : StaticBody2D, IHittable
{
    public bool Hit(int damage)
    {
        GetNode<AudioStreamPlayer2D>("HitSoundPlayer").Play();
        return false;
    }
    
}
