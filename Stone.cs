using Godot;
using System;

public class Stone : StaticBody2D, IHittable
{
    [Export] public int Health = 5;
    [Export] public PackedScene Loot;

    [Signal]
    public delegate void Cracked(Vector2 position);

    public bool Hit(int damage)
    {
        GetNode<AudioStreamPlayer2D>("HitSoundPlayer").Play();
        Health -= damage;
        if (Health <= 0)
        {
            EmitSignal(nameof(Cracked), Position);
            if (Loot != null)
            {
                var loot = (Node2D)Loot.Instance();
                loot.Position = Position;
                GetParent().AddChild(loot);
            }
            QueueFree();
        }
        return true;
    }
}
