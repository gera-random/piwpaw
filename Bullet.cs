using Godot;

public class Bullet : Projectile
{

    [Export] private PackedScene HitFlare;

    public override void OnCollision(KinematicCollision2D collision)
    {
        _velocity = _velocity.Bounce(collision.Normal);
        if (collision.Collider.HasMethod("Hit"))
        {
            var destroyBullet = (bool) collision.Collider.Call("Hit", Damage);

            var hitFlare = (Particles2D) HitFlare.Instance();
            hitFlare.OneShot = true;
            hitFlare.Emitting = true;
            hitFlare.Position = Position;
            GetParent().AddChild(hitFlare);

            if (destroyBullet)
                QueueFree();
        }
    }
}