﻿using Godot;

public abstract class Projectile : KinematicBody2D
{
    [Export] public int Speed = 750;
    protected Vector2 _velocity;
    [Export] public int Cost = 1;
    [Export] public int Damage = 1;
    [Export] public PackedScene Pickable;
    
    public void Start(Vector2 pos, float dir)
    {
        Rotation = dir;
        Position = pos;
        _velocity = new Vector2(Speed, 0).Rotated(Rotation);
    }
    
    public override void _PhysicsProcess(float delta)
    {
        var collision = MoveAndCollide(_velocity * delta);
        if (collision != null)
        {
            OnCollision(collision);
        }
    }
    
    public void OnVisibilityNotifier2DScreenExited()
    {
        QueueFree();
    }
    
    public abstract void OnCollision(KinematicCollision2D collision);
}
