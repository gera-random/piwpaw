using Godot;

public class GhostSpawner : Area2D
{
    [Export] public PackedScene GhostScene = (PackedScene) GD.Load("res://Ghost.tscn");
    
    public void OnBodyEntered(Object body)
    {
        if (body.HasMethod(nameof(Player.TakeDamage)))
        {
            var ghostInstance1 = (Node2D) GhostScene.Instance();
            var ghostInstance2 = (Node2D) GhostScene.Instance();
            var ghostInstance3 = (Node2D) GhostScene.Instance();

            ghostInstance1.GlobalPosition = GetNode<Position2D>("P1").GlobalPosition;
            ghostInstance2.GlobalPosition = GetNode<Position2D>("P2").GlobalPosition;
            ghostInstance3.GlobalPosition = GetNode<Position2D>("P3").GlobalPosition;

            ghostInstance1.Connect(nameof(Ghost.Defeated), Owner, nameof(GameWorld.OnGhostDefeated));
            ghostInstance2.Connect(nameof(Ghost.Defeated), Owner, nameof(GameWorld.OnGhostDefeated));
            ghostInstance3.Connect(nameof(Ghost.Defeated), Owner, nameof(GameWorld.OnGhostDefeated));

            Owner.AddChild(ghostInstance1);
            Owner.AddChild(ghostInstance2);
            Owner.AddChild(ghostInstance3);

            QueueFree();
        }
    }
}
